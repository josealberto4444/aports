# Contributor: Alexandru Campeanu <tiotags1@gmail.com>
# Maintainer: Alexandru Campeanu <tiotags1@gmail.com>
pkgname=hinsightd
pkgver=0.9.8
pkgrel=0
pkgdesc="hinsightd a http/1.1 webserver with (hopefully) minimal goals"
url="https://gitlab.com/tiotags/hin9"
arch="all"
license="BSD-3-Clause"
makedepends="cmake libcap lua-dev openssl-dev zlib-dev liburing-dev linux-headers"
options="!check" # no test suite
pkgusers="$pkgname"
pkggroups="$pkgname"
subpackages="$pkgname-dbg $pkgname-openrc"
source="
	https://gitlab.com/tiotags/hin9/-/archive/v$pkgver/hin9-v$pkgver.tar.gz
	"
builddir="$srcdir"/hin9-v$pkgver

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=None \
		$CMAKE_CROSSOPTS .
	cmake --build build
}

package() {
	install -D -m755 "$builddir"/build/hin9 \
		"$pkgdir"/usr/sbin/hinsightd
	setcap cap_net_bind_service+eip "$pkgdir"/usr/sbin/hinsightd

	# create dirs
	install -d -m2750 -o $pkgusers -g $pkggroups \
		"$pkgdir"/var/log/hinsightd/
	install -d -m755 \
		"$pkgdir"/etc/hinsightd/ \
		"$pkgdir"/var/www/localhost/htdocs

	install -m755 -D "$builddir"/external/packaging/$pkgname.initd.sh \
		"$pkgdir"/etc/init.d/$pkgname
	install -m644 -D "$builddir"/external/packaging/$pkgname.confd.sh \
		"$pkgdir"/etc/conf.d/$pkgname
	install -m644 -D "$builddir"/external/packaging/$pkgname.logrotate.sh \
		"$pkgdir"/etc/logrotate.d/$pkgname

	# config files
	local i; for i in default_config.lua main.lua lib.lua
	do
		install -m644 "$builddir"/workdir/"$i" "$pkgdir"/etc/$pkgname/"$i"
	done

}

sha512sums="
3a408ff085ba4c7f7d72edbbe306d9b5db50fc2d5951adb92befbed3ca86e2b610713b370978a9003fd087d4337165ae3ecf650f90047d5c4ffe217e742e4117  hin9-v0.9.8.tar.gz
"
