# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=borgmatic
pkgver=1.5.17
pkgrel=0
pkgdesc="Simple, configuration-driven backup software for servers and workstations"
url="https://torsion.org/borgmatic/"
arch="noarch !s390x !mips !mips64 !armhf" # limited by borgbackup
license="GPL-3.0-or-later"
depends="
	borgbackup
	python3
	py3-setuptools
	py3-jsonschema
	py3-requests
	py3-ruamel.yaml
	py3-colorama
	"
checkdepends="
	py3-pytest
	py3-pytest-cov
	py3-flexmock
	"
source="$pkgname-$pkgver.tar.gz::https://projects.torsion.org/witten/borgmatic/archive/$pkgver.tar.gz"
builddir="$srcdir/borgmatic"

build() {
	python3 setup.py build
}

check() {
	# omit a simple test that requires borgmatic to be available in $PATH
	pytest -k "not test_borgmatic_version_matches_news_version"
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir" --skip-build
}

sha512sums="
40e5befb7d559da411eae78bdaf114cfec1b424e71d244c3f11c84c1a38b4b76ce5e098cfbb8445dde178f7dc933ced0fb3d709ee34ec489db14c5917fc3a0f9  borgmatic-1.5.17.tar.gz
"
